from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.utils.translation import gettext_lazy as _
from django.contrib.auth import get_user_model
from .models import Order, Customer

User = get_user_model()


class OrderForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        # встановлення надпису над полем
        super().__init__(*args, **kwargs)
        self.fields['order_date'].label = 'Дата отримання замовлення'

    # присвоєння віджету
    order_date = forms.DateField(widget=forms.TextInput(attrs={'type': 'date'}))

    class Meta:
        model = Order
        fields = (
            'first_name', 'last_name', 'phone', 'address', 'buying_type', 'comment', 'order_date'
        )


class RegistrationForm(UserCreationForm):

    confirm_password = forms.CharField(widget=forms.PasswordInput)
    phone = forms.CharField(max_length=12, required=False)
    address = forms.CharField(max_length=500, required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['username'].label = 'Логін'
        self.fields['password'].label = 'Пароль'
        self.fields['confirm_password'].label = 'Підтвердіть пароль'
        self.fields['phone'].label = 'Номер телефону'
        self.fields['first_name'].label = "Ім'я"
        self.fields['last_name'].label = 'Прізвище'
        self.fields['address'].label = 'Адреса'
        self.fields['email'].label = 'Електронна пошта'

    class Meta(UserCreationForm.Meta):
        model = User
        fields = ('username', 'password', 'confirm_password', 'first_name', 'last_name', 'email', 'address', 'phone')
