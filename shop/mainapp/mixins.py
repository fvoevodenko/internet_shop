from django.views.generic.detail import SingleObjectMixin, BaseDetailView
from django.views.generic import View
from .models import Category, Cart, Customer, Accessories, Cosmetics, Whiteness


class CategoryDetailMixin(BaseDetailView):

    CATEGORY_SLUG2PRODUCT_MODEL = {
        'accessories': Accessories,
        'cosmetics': Cosmetics,
        'whiteness': Whiteness
    }

    def get_context_data(self, **kwargs):
        # отримання інформації, яку буде показано в лівій панелі сторінок категорій
        # щоб розрізнити запит по категорії від окремого продукту
        if isinstance(self.get_object(), Category):
            # отримуємо назву моделі
            model = self.CATEGORY_SLUG2PRODUCT_MODEL[self.get_object().slug]
            context = super().get_context_data(**kwargs)
            context['categories'] = Category.objects.get_categories_for_left_sidebar()
            context['category_products'] = model.objects.all()
            # якщо є запит
            if self.request:
                request_dict = dict(self.request.GET.lists())
                # якщо є запит з параметрами фільтру
                if len(request_dict) > 0:
                    command_str = "context['category_products'] = model.objects.filter("
                    # print('len request', len(request_dict))
                    # print(request_dict)
                    count_filter = len(request_dict)
                    for key in request_dict:
                        command_str = command_str + str(key)
                        count_filter -= 1
                        if len(request_dict[key]) == 1:
                            command_str += "='"
                        else:
                            command_str += "__in=['"
                        count_parameter = len(request_dict[key])
                        for parameter in request_dict[key]:
                            command_str += parameter
                            count_parameter -= 1
                            if count_filter == 0 and len(request_dict[key]) == 1 and count_parameter == 0:
                                command_str += "')"
                            elif count_filter == 0 and len(request_dict[key]) > 1 and count_parameter == 0:
                                command_str += "'])"
                            elif count_filter > 0 and len(request_dict[key]) > 1 and count_parameter == 0:
                                command_str += "'], "
                            elif count_filter > 0 and len(request_dict[key]) == 1 and count_parameter == 0:
                                command_str += "', "
                            elif count_parameter > 0:
                                command_str += "', '"
                    # print(command_str)
                    exec(command_str)
                    return context

            # для категорії без фільтру
            return context
        # для окремого продукту
        context = super().get_context_data(**kwargs)
        context['categories'] = Category.objects.get_categories_for_left_sidebar()
        return context


class CartMixin(View):
    # дозволяє розрізняти користувачів по статусу реєстрації, не зареєстровані також можуть купувати
    def dispatch(self, request, *args, **kwargs):
        # якщо користувач авторізований
        if request.user.is_authenticated:
            # шукаємо його
            customer = Customer.objects.filter(user=request.user).first()
            # створюємо, якщо не знайдено користувача
            if not customer:
                customer = Customer.objects.create(user=request.user)
            # та його кошик
            cart = Cart.objects.filter(owner=customer, in_order=False).first()
            # якщо кошик знайдено
            if not cart:
                # створюємо новий
                cart = Cart.objects.create(owner=customer)
        # якщо користувач не авторизований
        else:
            # шукаємо його кошик
            cart = Cart.objects.filter(for_anonymous_user=True).first()
            if not cart:
                cart = Cart.objects.create(for_anonymous_user=True)
        self.cart = cart
        # повертаємо результат роботи методу
        return super().dispatch(request, *args, **kwargs)
