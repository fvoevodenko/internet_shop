# Generated by Django 4.1.6 on 2023-03-09 19:02

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('mainapp', '0009_alter_customer_phone_alter_order_phone'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cartproduct',
            name='user',
            field=models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='mainapp.customer', verbose_name='Покупець'),
        ),
    ]
