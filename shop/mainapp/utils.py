from django.db import models


def recalc_cart(cart):
    # розрахунок кількості та вартості товарів в кошику
    cart_data = cart.products.aggregate(models.Sum('final_price'), models.Count('id'))
    if cart_data.get('final_price__sum'):
        cart.final_price = cart_data['final_price__sum']
    else:
        # щоб програма не присвоїла значення Null
        cart.final_price = 0
    cart.total_products = cart_data['id__count']
    cart.save()
