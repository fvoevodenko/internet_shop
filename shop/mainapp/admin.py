from django.contrib import admin
from django.forms import ModelChoiceField, ModelForm, ValidationError
from django.utils.safestring import mark_safe
from django import forms
from .models import *
from PIL import Image
from django.urls import path
from .views import product_report


class SizeAdminForm(ModelForm):
    # клас для перевірки розміру завантажуваних зображень

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['image'].help_text = mark_safe(
            f'<span style="color:red; font-size:14px;">Завантажуйте зображення з розміром більше'
            f' {Product.MIN_RESOLUTION[0]}x{Product.MIN_RESOLUTION[1]}'
            f' та менше {Product.MAX_RESOLUTION[0]}x{Product.MAX_RESOLUTION[1]} </span>'
        )

    def clean_image(self):
        # перевірка розмірів зображення
        # файл зображення
        image = self.cleaned_data['image']
        # кортеж зі значеннями роздільної здатності файлу зображення
        img = Image.open(image)
        min_height, min_width = Product.MIN_RESOLUTION
        max_height, max_width = Product.MAX_RESOLUTION
        # перевірка максимального розміру файлу
        if image.size > Product.MAX_IMAGE_SIZE:
            raise ValidationError('Розмір зображення не повинен перевищувати 6МБ!')
        if img.height < min_height or img.width < min_width:
            raise ValidationError('Розмір зображення меньше мінімального!')
        if img.height > max_height or img.width > max_width:
            raise ValidationError('Розмір зображення більше максимального!')
        return image


class WhitenessAdmin(admin.ModelAdmin):

    form = SizeAdminForm

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        # для уникнення вибору інших категорій при вводі товару
        if db_field.name == 'category':
            return ModelChoiceField(Category.objects.filter(slug='whiteness'))
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


class CosmeticsAdmin(admin.ModelAdmin):

    form = SizeAdminForm

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        # для уникнення вибору інших категорій при вводі товару
        if db_field.name == 'category':
            return ModelChoiceField(Category.objects.filter(slug='cosmetics'))
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


class AccessoriesAdmin(admin.ModelAdmin):

    form = SizeAdminForm

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        # для уникнення вибору інших категорій при вводі товару
        if db_field.name == 'category':
            return ModelChoiceField(Category.objects.filter(slug='accessories'))
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


class MyAdminSite(admin.AdminSite):
    site_header = 'My Site Administration'
    site_title = 'My Site'

    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path('product-report/', self.admin_view(product_report), name='product_report'),
        ]
        return my_urls + urls


admin_site = MyAdminSite(name='myadmin')


# реєстрація моделей
admin.site.register(Category)
admin.site.register(CartProduct)
admin.site.register(Cart)
admin.site.register(Customer)
admin.site.register(Whiteness, WhitenessAdmin)
admin.site.register(Cosmetics, CosmeticsAdmin)
admin.site.register(Accessories, AccessoriesAdmin)
admin.site.register(Order)
