from django.contrib.auth import authenticate, login
from django.contrib.contenttypes.models import ContentType
from django.shortcuts import render
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib import messages
from django.shortcuts import render
from django.views.generic import DetailView, View, CreateView
from .models import Whiteness, Cosmetics, Accessories, Category, LatestProducts, Customer, Cart, CartProduct
from .mixins import CategoryDetailMixin, CartMixin
from django.http import HttpResponseRedirect, HttpResponse
from .forms import OrderForm, RegistrationForm
from .utils import recalc_cart
from django.db import transaction
from queryset_sequence import QuerySetSequence


class BaseView(CartMixin, View):
    # відображення головної сторінки

    def get(self, request, *args, **kwargs):
        # інформація в лівій панелі
        categories = Category.objects.get_categories_for_left_sidebar()
        # інформація в головній панелі
        products = LatestProducts.objects.get_product_for_main_page(
            'accessories', 'cosmetics', 'whiteness', with_respect_to='cosmetics')
        context = {
            'categories': categories,
            'products': products,
            'cart': self.cart
        }
        return render(request, 'base.html', context)


class ProductDetailView(CartMixin, CategoryDetailMixin, DetailView):
    # клас для звернення до обраного продукту

    CT_MODEL_CLASS = {
        'whiteness': Whiteness,
        'cosmetics': Cosmetics,
        'accessories': Accessories
    }

    def dispatch(self, request, *args, **kwargs):
        # дозволяє отримати необхідну модель за допомогою стандартного методу dispatch
        self.model = self.CT_MODEL_CLASS[kwargs['ct_model']]
        self.queryset = self.model._base_manager.all()
        return super().dispatch(request, *args, **kwargs)

    context_object_name = 'product'
    template_name = 'product_detail.html'
    slug_url_kwarg = 'slug'

    def get_context_data(self, **kwargs):
        # інформація, що виводиться в шаблоні
        context = super().get_context_data(**kwargs)
        context['ct_model'] = self.model._meta.model_name
        context['cart'] = self.cart
        return context


class CategoryDetailView(CartMixin, CategoryDetailMixin, DetailView):
    # клас для створення посилань на категорії
    model = Category
    queryset = Category.objects.all()
    context_object_name = 'category'
    template_name = 'category_detail.html'
    slug_url_kwarg = 'slug'

    def get_context_data(self, **kwargs):
        # інформація, що виводиться в шаблоні
        context = super().get_context_data(**kwargs)
        context['cart'] = self.cart
        return context


class AddToCartView(CartMixin, View):
    def get(self, request, *args, **kwargs):
        # беремо товари
        ct_model, product_slug = kwargs.get('ct_model'), kwargs.get('slug')
        # визначаємо модель товару
        content_type = ContentType.objects.get(model=ct_model)
        # отримуємо товар через звернення до батьківського класу
        product = content_type.model_class().objects.get(slug=product_slug)
        # шукаємо об'єкт в кошику або створюємо, якщо не знайшли
        cart_product, created = CartProduct.objects.get_or_create(
            user=self.cart.owner, cart=self.cart, content_type=content_type, object_id=product.id
        )
        # додаємо товар в кошик, якщо не знайшли
        if created:
            self.cart.products.add(cart_product)
        # обновляємо інформацію, після додавання товару
        recalc_cart(self.cart)
        # повідомлення про додавання товару
        messages.add_message(request, messages.INFO, 'Товар успішно добавлено до кошику')
        return HttpResponseRedirect('/cart/')


class DeleteFromCartView(CartMixin, View):
    def get(self, request, *args, **kwargs):
        # беремо товари
        ct_model, product_slug = kwargs.get('ct_model'), kwargs.get('slug')
        # визначаємо модель товару
        content_type = ContentType.objects.get(model=ct_model)
        # отримуємо товар через звернення до батьківського класу
        product = content_type.model_class().objects.get(slug=product_slug)
        # шукаємо об'єкт в кошику
        cart_product = CartProduct.objects.get(
            user=self.cart.owner, cart=self.cart, content_type=content_type, object_id=product.id
        )
        # видаляємо товар з кошику
        self.cart.products.remove(cart_product)
        # видалення товару з CartProduct
        cart_product.delete()
        # обновляємо інформацію, після додавання товару
        recalc_cart(self.cart)
        messages.add_message(request, messages.INFO, 'Товар видалено з кошику')
        return HttpResponseRedirect('/cart/')


class CartView(CartMixin, View):
    def get(self, request, *args, **kwargs):
        categories = Category.objects.get_categories_for_left_sidebar()
        context = {
            'cart': self.cart,
            'categories': categories
        }
        return render(request, 'cart.html', context)


class CheckoutView(CartMixin, View):
    # виведення шаблону замовлення
    def get(self, request, *args, **kwargs):
        categories = Category.objects.get_categories_for_left_sidebar()
        form = OrderForm(request.POST or None)
        context = {
            'cart': self.cart,
            'categories': categories,
            'form': form
        }
        return render(request, 'checkout.html', context)


class MakeOrderView(CartView, View):
    # обробка замовлення
    # декоратор повертає стан до помилки при роботі з базою
    @transaction.atomic
    def post(self, request, *args, **kwargs):
        form = OrderForm(request.POST or None)
        customer = Customer.objects.get(user=request.user)
        if form.is_valid():
            new_order = form.save(commit=False)
            new_order.customer = customer
            new_order.first_name = form.cleaned_data['first_name']
            new_order.last_name = form.cleaned_data['last_name']
            new_order.phone = form.cleaned_data['phone']
            new_order.address = form.cleaned_data['address']
            new_order.buying_type = form.cleaned_data['buying_type']
            new_order.order_date = form.cleaned_data['order_date']
            new_order.comment = form.cleaned_data['comment']
            new_order.save()
            self.cart.in_order = True
            self.cart.save()
            new_order.cart = self.cart
            new_order.save()
            customer.orders.add(new_order)
            messages.add_message(request, messages.INFO, 'Дякуємо за покупку!')
            return HttpResponseRedirect('/')
        return HttpResponseRedirect('/checkout/')


class RegistrationView(CartMixin, View):

    def get(self, request, *args, **kwargs):
        form = RegistrationForm(request.POST or None)
        categories = Category.objects.all()
        context = {
            'form': form,
            'categories': categories,
            'cart': self.cart
        }
        return render(request, 'registration.html', context)

    def post(self, request, *args, **kwargs):
        form = RegistrationForm(request.POST or None)
        if form.is_valid():
            new_user = form.save(commit=False)
            new_user.username = form.cleaned_data['username']
            new_user.email = form.cleaned_data['email']
            new_user.first_name = form.cleaned_data['first_name']
            new_user.last_name = form.cleaned_data['last_name']
            new_user.save()
            new_user.set_password(form.cleaned_data['password'])
            new_user.save()
            Customer.objects.create(
                user=new_user,
                phone=form.cleaned_data['phone'],
                address=form.cleaned_data['address']
            )
            user = authenticate(
                username=new_user.username, password=form.cleaned_data['password']
            )
            login(request, user)
            return HttpResponseRedirect('/')
        categories = Category.objects.all()
        context = {
            'form': form,
            'categories': categories,
            'cart': self.cart
        }
        return render(request, 'registration.html', context)


@staff_member_required
def product_report(request):

    title_value = request.GET.get('title')
    price_value = request.GET.get('price')
    category_value = request.GET.get('category')

    data1 = Accessories.objects.all()
    data2 = Cosmetics.objects.all()
    data3 = Whiteness.objects.all()

    data = QuerySetSequence(data1, data2, data3)

    if title_value:
        data = data.filter(title__contains=title_value)
    if price_value:
        data = data.filter(price=price_value)
    if category_value:
        data = data.filter(category__name__contains=category_value)

    price_sum = 0
    for i in range(len(data)):
        price_sum += data[i].price

    content = {'data': data, 'price_sum': price_sum}

    return render(request, 'admin/mainapp/product_report.html', content)
