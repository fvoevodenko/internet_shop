from django.db import models
from django.contrib.auth import get_user_model
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey
from PIL import Image
from django.urls import reverse
from django.utils import timezone

User = get_user_model()


def get_models_for_count(*model_names):
    # рахує кількість моделей
    return [models.Count(model_name) for model_name in model_names]


def get_product_url(obj, viewname):
    ct_model = obj.__class__._meta.model_name
    return reverse(viewname, kwargs={'ct_model': ct_model, 'slug': obj.slug})


class MinResolutionErrorException(Exception):
    pass


class MaxResolutionErrorException(Exception):
    pass


class LatestProductsManager:
    # імітація класів останніх надходжень для головної сторінки
    @staticmethod
    def get_product_for_main_page(*args, **kwargs):
        # змінна для встановлення пріоритетної категорії надходжень
        with_respect_to = kwargs.get('with_respect_to')
        products = []
        # запит останніх надходжень
        ct_models = ContentType.objects.filter(model__in=args)
        # ітерування по результату запиту
        for ct_model in ct_models:
            # звернення до батьківського класу від типу
            model_products = ct_model.model_class()._base_manager.all().order_by('-id')[:5]
            # створюємо список моделей
            products.extend(model_products)
        # перевірка, чи передано аргументи пріоритетної категорії
        if with_respect_to:
            # якщо аргумент передано, знаходимо необхідну модель
            ct_model = ContentType.objects.filter(model=with_respect_to)
            # перевіряємо, чи переданий аргумент коректний (присутній в звичайних аргументах)
            if ct_model.exists():
                if with_respect_to in args:
                    # сортуємо список
                    return sorted(
                        products, key=lambda x: x.__class__._meta.model_name.startswith(with_respect_to), reverse=True)
        return products


class LatestProducts:
    # імітація поведінки стандартної моделі
    objects = LatestProductsManager()


class CategoryManager(models.Manager):

    # кількість товару по категоріям
    CATEGORY_NAME_COUNT_NAME = {
        'Аксесуари': 'accessories__count',
        'Косметика': 'cosmetics__count',
        'Білизна': 'whiteness__count'
    }

    def get_queryset(self):
        return super().get_queryset()

    def get_categories_for_left_sidebar(self):
        models = get_models_for_count('accessories', 'cosmetics', 'whiteness')
        qs = list(self.get_queryset().annotate(*models))
        data = [dict(name=c.name, url=c.get_absolute_url(),
                     count=getattr(c, self.CATEGORY_NAME_COUNT_NAME[c.name])) for c in qs]
        return data

# моделі
# 1 Category Категорія
# 2 Product Продукт
# 3 CartProduct Проміжний продукт (для корзини)
# 4 Cart Корзина
# 5 Order Замовлення

# 6 Customer Покупець
# 7 Specification Характеристики


class Category(models.Model):
    name = models.CharField(max_length=255, verbose_name="Ім'я категорії")
    slug = models.SlugField(unique=True)
    objects = CategoryManager()

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('category_detail', kwargs={'slug': self.slug})


class Product(models.Model):
    MIN_RESOLUTION = (400, 400)
    MAX_RESOLUTION = (4000, 6000)
    MAX_IMAGE_SIZE = 3145728 * 2

    class Meta:
        abstract = True

    category = models.ForeignKey(Category, verbose_name='Категорія', on_delete=models.CASCADE)
    title = models.CharField(max_length=255, verbose_name='Найменування')
    slug = models.SlugField(unique=True)
    image = models.ImageField(verbose_name='Зображення')
    description = models.TextField(verbose_name='Опис', null=True)
    price = models.DecimalField(max_digits=9, decimal_places=0, verbose_name='Ціна')
    producer = models.CharField(max_length=50, verbose_name='Виробник')
    country = models.CharField(max_length=50, verbose_name='Країна виробництва')

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        # перевірка розмірів зображення
        image = self.image
        # кортеж зі значеннями роздільної здатності файлу зображення
        img = Image.open(image)
        min_height, min_width = self.MIN_RESOLUTION
        max_height, max_width = self.MAX_RESOLUTION
        if img.height < min_height or img.width < min_width:
            raise MinResolutionErrorException('Розмір зображення меньше мінімального!')
        if img.height > max_height or img.width > max_width:
            raise MaxResolutionErrorException('Розмір зображення більше максимального!')
        # для виключення помилки при визові зображення
        super().save(*args, **kwargs)

    def get_model_name(self):
        return self.__class__.__name__.lower()


class CartProduct(models.Model):
    user = models.ForeignKey('Customer', verbose_name='Покупець', on_delete=models.CASCADE, blank=True, null=True)
    # обернене відношення
    cart = models.ForeignKey('Cart', verbose_name='Корзина', on_delete=models.CASCADE, related_name='related_products')
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    qty = models.PositiveIntegerField(default=1)
    final_price = models.DecimalField(max_digits=9, decimal_places=0, verbose_name='Сума')

    def __str__(self):
        return f'Товар: {self.content_object.title} для кошику'

    def save(self, *args, **kwargs):
        self.final_price = self.qty * self.content_object.price
        super().save(*args, **kwargs)


class Cart(models.Model):
    # власник кошику з null=True може бути не зареєстрованим
    owner = models.ForeignKey('Customer', null=True, verbose_name='Власник', on_delete=models.CASCADE)
    # обернене відношення
    products = models.ManyToManyField(CartProduct, blank=True, related_name='related_cart')
    total_products = models.PositiveIntegerField(default=0)
    final_price = models.DecimalField(max_digits=9, default=0, decimal_places=0, verbose_name='Сума')
    # позначаємо, що корзина зайнята користувачем, коли True
    in_order = models.BooleanField(default=False)
    # для неавторизованих користувачів
    for_anonymous_user = models.BooleanField(default=False)

    def __str__(self):
        return str(self.id)


class Customer(models.Model):
    user = models.ForeignKey(User, verbose_name='Користувач', on_delete=models.CASCADE)
    # не обов'язкові поля для незареєстрованих користувачів
    phone = models.CharField(max_length=12, verbose_name='Номер телефону', null=True, blank=True)
    # не обов'язкові поля для незареєстрованих користувачів
    address = models.CharField(max_length=255, verbose_name='Адреса', null=True, blank=True)
    orders = models.ManyToManyField('Order', verbose_name='Замовлення користувача', related_name='related_customer')

    def __str__(self):
        return f"Покупець {self.user.first_name} {self.user.last_name}"


# білизна(труси, бюстгалтери) - розмір
# косметика(парфумовані місти, гель для душу, блиск для губ)
# аксесуари(сумки, косметички)


class Whiteness(Product):
    type_whiteness = models.CharField(max_length=50, verbose_name='Тип білизни')
    size = models.CharField(max_length=50, verbose_name='Розмір білизни')

    def __str__(self):
        return f"{self.category.name} {self.title}"

    def get_absolute_url(self):
        return get_product_url(self, 'product_detail')


class Cosmetics(Product):
    type_cosmetics = models.CharField(max_length=50, verbose_name='Тип косметики')

    def __str__(self):
        return f"{self.category.name} {self.title}"

    def get_absolute_url(self):
        return get_product_url(self, 'product_detail')


class Accessories(Product):
    type_accessories = models.CharField(max_length=50, verbose_name='Тип аксесуару')

    def __str__(self):
        return f"{self.category.name} {self.title}"

    def get_absolute_url(self):
        return get_product_url(self, 'product_detail')


class Order(models.Model):

    STATUS_NEW = 'new'
    STATUS_IN_PROGRESS = 'in_progress'
    STATUS_READY = 'is_ready'
    STATUS_COMPLETED = 'completed'

    BUYING_TYPE_SELF = 'self'
    BUYING_TYPE_DELIVERY = 'delivery'

    STATUS_CHOICES = (
        (STATUS_NEW, 'Нове замовлення'),
        (STATUS_IN_PROGRESS, 'Замовлення в обробці'),
        (STATUS_READY, 'Замовлення готове'),
        (STATUS_COMPLETED, 'Замовлення виконано')
    )

    BUYING_TYPE_CHOICES = (
        (BUYING_TYPE_SELF, 'Самовивіз'),
        (BUYING_TYPE_DELIVERY, 'Доставка')
    )

    customer = models.ForeignKey(
        Customer, verbose_name='Покупець', related_name='related_orders', on_delete=models.CASCADE
    )
    first_name = models.CharField(max_length=50, verbose_name="Ім'я")
    last_name = models.CharField(max_length=50, verbose_name="Прізвище")
    phone = models.CharField(max_length=12, verbose_name='Номер телефону')
    cart = models.ForeignKey(Cart, verbose_name='Кошик', on_delete=models.CASCADE, null=True, blank=True)
    # не обов'язкове поле для незареєстрованих користувачів
    address = models.CharField(max_length=255, verbose_name='Адреса', null=True, blank=True)
    status = models.CharField(
        max_length=100, verbose_name='Статус замовлення', choices=STATUS_CHOICES, default=STATUS_NEW
    )
    buying_type = models.CharField(
        max_length=100,
        verbose_name='Тип замовлення',
        choices=BUYING_TYPE_CHOICES,
        default=BUYING_TYPE_SELF
    )
    comment = models.TextField(verbose_name='Коментар до замовлення', null=True, blank=True)
    created_at = models.DateTimeField(auto_now=True, verbose_name='Дата створення замовлення')
    order_date = models.DateField(verbose_name='Дата отримання замовлення', default=timezone.now)

    def __str__(self):
        return str(self.id)
